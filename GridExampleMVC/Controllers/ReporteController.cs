﻿using GridExampleMVC.Models;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Collections;
using ClosedXML.Excel;
using System.Data;

namespace GridExampleMVC.Controllers
{
    public class ReporteController : Controller
    {

        private DB_DCMEntities db = new DB_DCMEntities();


        // GET: Asset
        public ActionResult Defectos()
        {
            DateTime date1 = DateTime.Now.AddDays(-7);
            DateTime date2 = DateTime.Now;
            string sord = "%";
            return View(db.DCM_ReportGeneral(sord, date1,date2));
        }

        // GET: Asset
        public ActionResult Productividad()
        {
            DateTime date1 = DateTime.Now.AddDays(-7);
            DateTime date2 = DateTime.Now;
            return View(db.DCM_ReportProductivity("%","%","%","%","%",date1,date2));
        }


        // GET: Asset
        public ActionResult ProdTiempoReal()
        {
            DateTime date1 = DateTime.Now;
            return View(db.DCM_ReportLineProdBETA(7,1,date1));
        }

        [HttpPost]
        public ActionResult Defectos(string sord, string finicial, string ffinal)
        {
            if (string.IsNullOrEmpty(sord))
            {
                sord = "%";
            }
            if (string.IsNullOrEmpty(finicial))
            {
                DateTime date1 = DateTime.Now.AddDays(-7);
                finicial = date1.ToString();
            }

            if (string.IsNullOrEmpty(ffinal))
            {
                DateTime date2 = DateTime.Now;
                ffinal = date2.ToString();
            }

            return View(db.DCM_ReportGeneral(sord, DateTime.Parse(finicial), DateTime.Parse(ffinal)));
        }

        [HttpPost]
        public ActionResult Productividad(string finicial, string ffinal)
        {
            if (string.IsNullOrEmpty(finicial))
            {
                DateTime date1 = DateTime.Now.AddDays(-7);
                finicial = date1.ToString();
            }

            if (string.IsNullOrEmpty(ffinal))
            {
                DateTime date2 = DateTime.Now;
                ffinal = date2.ToString();
            }

            return View(db.DCM_ReportProductivity("%", "%", "%", "%", "%", DateTime.Parse(finicial), DateTime.Parse(ffinal)));
        }



    }
}